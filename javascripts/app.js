$(function(){
   $("#toggleGraphVertexes").on('click', function () {
       toggleVisibility($('#graphVertexes'));
   });

    $("#toggleKruskalOutput").on('click', function () {
        toggleVisibility($('#kruskalOutput'));
    });
});

function toggleVisibility(Element) {
    if (Element.is(":visible")) {
        Element.hide();
    } else {
        Element.show();
    }
}