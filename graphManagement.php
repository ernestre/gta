<?php

if (!empty($_POST)) {
    if (!empty($_POST['newVertexCount'])) {
        for ($i = 1; $i <= $_POST['newVertexCount']; $i++) {
            $vertex = new \Classes\Vertex();
            $vertex->setId(sizeof($graph->getVertexes()));
            $graph->addVertex($vertex);
        }
    }

    if (isset($_POST['edgeFrom']) && isset($_POST['edgeTo'])) {
        $graph->addEdgeNode((int)$_POST['edgeFrom'], (int)$_POST['edgeTo']);
    }

    if (isset($_POST['delEdgeFrom']) && isset($_POST['delEdgeTo'])) {
        $graph->deleteEdgeNode((int)$_POST['delEdgeFrom'], (int)$_POST['delEdgeTo']);
    }

    if (isset($_POST['deleteVertexById'])) {
        $graph->deleteVertex((int)$_POST['deleteVertexById']);
    }


    if (!empty($_POST['optionsRadios'])) {
        if ($_POST['optionsRadios'] == 'directed') {
            $graph->setDirected(true);
        } else {
            $graph->setDirected(false);
        }
    }
    header("Location: display.php");
}