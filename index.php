<?php
session_start();
session_destroy();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Grafu algoritmu teorija</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="page-header">
                    <h3>Graph Generation</h3>
                </div>

                <form action="generate.php" method="POST" class="form-horizontal">

                    <div class="form-group">
                        <label for="vertexCount" class="col-sm-3  control-label">Vertex Count</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" name="vertexCount" min="1" id="vertexCount">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="minEdges" class="col-sm-3  control-label">Minimum edges</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" name="minEdges" min="1" id="minEdges">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="maxEdges" class="col-sm-3  control-label">Maximum Edges</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" name="maxEdges" min="1" id="maxEdges">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="directed" checked>
                                    Graph should bet directed
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-9 col-sm-offset-3">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="undirected">
                                    Graph should bet undirected
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Generate</button>
                        </div>
                    </div>

                </form>

                <div class="page-header">
                    <h3>Load graph</h3>
                </div>

                <form action="generate.php" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label for="fileName" class="col-sm-3">File location (Can be restful API) : </label>
                        <div class="col-sm-9">
                            <input type="text" name="fileName" class="form-control" id="fileName"">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Load</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</body>
</html>