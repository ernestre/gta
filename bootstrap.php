<?php

require_once "Classes/Graph.php";
require_once "Classes/Vertex.php";
require_once "Classes/EdgeNode.php";
require_once "Classes/DepthFirstSearch.php";
require_once "Classes/Kruskal.php";
