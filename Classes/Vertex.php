<?php

namespace Classes;

class Vertex
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var EdgeNode[];
     */
    protected $edgeNodes;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return EdgeNode[]
     */
    public function getEdgeNodes()
    {
        return $this->edgeNodes;
    }

    /**
     * @param array $edgeNodes
     */
    public function setEdgeNodes($edgeNodes)
    {
        $this->edgeNodes = $edgeNodes;
    }

    /**
     * @param EdgeNode $edgeNode
     */
    public function addEdgeNode(EdgeNode $edgeNode)
    {
        $this->edgeNodes[] = $edgeNode;
    }

    /**
     * Deletes edge node if exists
     *
     * @param $vertexId
     */
    public function deleteEdgeNode($vertexId)
    {
        if (null !== $this->edgeNodes) {
            foreach ($this->edgeNodes as $key => $node) {
                if ($node->getVertex()->getId() == $vertexId) {
                    unset($this->edgeNodes[$key]);
                    return true;
                }
            }
        }
        return false;
    }
}
