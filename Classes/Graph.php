<?php

namespace Classes;

class Graph
{

    /**
     * How many times should I try to generate a graph?
     */
    const TRY_COUNT = 5;

    /**
     * @var boolean
     */
    private $directed;

    /**
     * @var int
     */
    private $tryCounter;

    /**
     * @var Vertex[]
     */
    protected $vertexes;

    /**
     * @return boolean
     */
    public function isDirected()
    {
        return $this->directed;
    }

    /**
     * @param boolean $directed
     */
    public function setDirected($directed)
    {
        $this->directed = $directed;
    }

    /**
     * Checks if two vertexes are neighbours.
     *
     * @param $vertexVId
     * @param $vertexUId
     */
    public function isAdjacent($vertexVId, $vertexUId)
    {
        $edgeNodes = $this->vertexes[$vertexVId]->getEdgeNodes();
        if (null !== $edgeNodes) {
            foreach ($edgeNodes as $node) {
                /** @var EdgeNode $node */
                if ($node->getVertex()->getId() == $vertexUId) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns array of neighbour vertexes
     *
     * @param $vertexId
     * @return EdgeNode[]
     */
    public function getNeighbours($vertexId)
    {
        if (sizeof($this->vertexes) - 1 > $vertexId && $vertexId >= 0) {
            return $this->vertexes[$vertexId]->getEdgeNodes();
        }

        throw new \OutOfBoundsException;
    }

    /**
     * Adds edge node to vertex if exists in the array.
     *
     * @param int $vertexXId VertexX
     * @param int $vertexYId VertexY(edge)
     * @param int $weight Edge_Weight
     * @return bool
     */
    public function addEdgeNode($vertexXId, $vertexYId, $weight = null)
    {
        $vertexX = $this->getVertexById($vertexXId);
        $vertexY = $this->getVertexById($vertexYId);

        if (!empty($vertexX->getEdgeNodes())) {
            foreach ($vertexX->getEdgeNodes() as $edge) {
                /** @var EdgeNode $edge */
                if ($edge->getVertex() == $vertexY) {
                    return false;
                }
            }
        }

        $edge = new EdgeNode();
        $edge->setVertex($vertexY);

        if (null !== $weight) {
            $edge->setWeight($weight);
        }

        $vertexX->addEdgeNode($edge);

        return true;
    }

    /**
     * Deletes edge node from vertex
     *
     * @param $vertexId
     * @param EdgeNode $edgeNode
     * @return $this
     */
    public function deleteEdgeNode($vertexXId, $vertexYId)
    {
        $vertex = $this->getVertexById($vertexXId);
        return $vertex->deleteEdgeNode($vertexYId);
    }

    /**
     * Adds vertex to the graph
     *
     * @param Vertex $vertex
     */
    public function addVertex(Vertex $vertex)
    {
        $this->vertexes[] = $vertex;
    }

    /**
     * Deletes vertex from the graph
     *
     * @param int $vertexId
     */
    public function deleteVertex($vertexId)
    {
        foreach ($this->vertexes as $vertex) {
            $vertex->deleteEdgeNode($vertexId);
        }
        unset($this->vertexes[$vertexId]);

        // Re-indexing vertexes
        $this->vertexes = array_values($this->vertexes);
        $size = sizeof($this->vertexes);
        for ($i = 0; $i < $size; $i++) {
            $this->vertexes[$i]->setId($i);
        }
    }

    /**
     * @return Vertex[]
     */
    public function getVertexes()
    {
        return $this->vertexes;
    }

    /**
     * @param Vertex[] $vertexes
     */
    public function setVertexes($vertexes)
    {
        $this->vertexes = $vertexes;
    }

    /**
     * Returns Vertex by array index
     *
     * @param $id
     * @return Vertex
     */
    public function getVertexById($id)
    {
        if ($id < sizeof($this->vertexes) && $id >= 0) {
            return $this->vertexes[$id];
        }

        throw new \OutOfBoundsException;
    }


    /**
     * @param Vertex $vertex
     * @param int $id
     */
    public function setVertexId(Vertex $vertex, $id)
    {
        // .. sets vertex id, we need to change the array order here...
    }

    /**
     * Generates a random graph.
     *
     * May generate directed or undirected graph.
     *
     * @param $vertexCount
     * @param $minEdges
     * @param $maxEdges
     * @param bool|false $directed
     */
    public function generateRandomGraph($vertexCount, $minEdges, $maxEdges, $directed = true)
    {
        // Checking arguments for invalid input
        foreach (func_get_args() as $arg) {
            if (is_null($arg)) {
                echo "arguments most not be null";
                return;
            }
        }

        if ($vertexCount < $minEdges) {
            echo "we cant generate this graph, because vertexCount < minEdges";
            return;
        } elseif ($vertexCount <= $maxEdges) {
            echo "we cant generate this graph, because vertexCount <= maxEdges";
            return;
        } elseif ($minEdges > $maxEdges) {
            echo "MinEdges can't be greater than maxEdges";
            return;
        }

        $this->directed = $directed;

        // Generating vertexes
        for ($i = 0; $i < $vertexCount; $i++) {
            $vertex = new Vertex();
            $vertex->setId($i);
            $this->vertexes[] = $vertex;
        }

        if ($directed === true) {
            $this->generateDirectedGraph($minEdges, $maxEdges);
        } else {
            $this->generateUndirectedGraph($vertexCount, $minEdges, $maxEdges);
        }
    }

    /**
     * Generates directed graph
     *
     * @param int $minEdges
     * @param int $maxEdges
     */
    protected function generateDirectedGraph($minEdges, $maxEdges)
    {
        $vertexes = $this->getVertexes();

        shuffle($vertexes);

        foreach ($vertexes as $vertex) {
            $edgesToGenerate = rand($minEdges, $maxEdges);
            $randomNumbers = range(0, $maxEdges);
            unset($randomNumbers[$vertex->getId()]);
            shuffle($randomNumbers);

            for ($i = 0; $i < $edgesToGenerate; $i++) {
                $weight = rand(1, 50);
                $this->addEdgeNode($vertex->getId(), $randomNumbers[$i], $weight);
            }
        }
    }

    /**
     * Generates undirected graph
     *
     * @param int $vertexCount
     * @param int $minEdges
     * @param int $maxEdges
     */
    protected function generateUndirectedGraph($vertexCount, $minEdges, $maxEdges)
    {
        $vertexes = $this->vertexes;
        // define graph generated status to false;
        $done = false;
        while ($done === false) {
            $done = true;
            shuffle($vertexes);

            // checking every vertex if it has less than minimum neighbours.
            foreach ($vertexes as $vertex) {
                if (sizeof($vertex->getEdgeNodes()) < $minEdges) {
                    // if it has less than minimum, we change state to false,
                    // this means, we need to add additional vertexes.
                    $done = false;
                    $vertexes = $this->vertexes;

                    // removing current vertex from vertex array so that we won't add it to itself.
                    unset($vertexes[array_search($vertex, $vertexes)]);
                    shuffle($vertexes);

                    // looking for a vertex which has 'maxEdges - 1' so that we can add it to the current vertex.
                    foreach ($vertexes as $edgeVertex) {
                        if (sizeof($edgeVertex->getEdgeNodes()) < $maxEdges &&
                            !$this->isAdjacent($vertex->getId(), $edgeVertex->getId())
                        ) {
                            $weight = rand(1, 50);
                            $this->addEdgeNode($vertex->getId(), $edgeVertex->getId(), $weight);
                            $this->addEdgeNode($edgeVertex->getId(), $vertex->getId(), $weight);
                            break;
                        }
                    }
                }
            }
        }

        // Checking if the generated graph is valid.
        foreach ($this->vertexes as $vertex) {
            $size = sizeof($vertex->getEdgeNodes());
            if ($size < $minEdges) {
                if (self::TRY_COUNT <= $this->tryCounter) {
                    echo "I FAILED TO GENERATE A GRAPH:( <BR>";
                    $this->vertexes = null;
                    return;
                }

                $this->tryCounter++;
                $this->vertexes = null;
                $this->generateRandomGraph($vertexCount, $minEdges, $maxEdges, false);
            }
        }
    }

    /**
     * Loads graph from a json file;
     *
     * @param $json
     */
    public function loadGraph($json)
    {
        $data = json_decode($json, true);

        // Using two loops because, we can't add for example vertex 7 , as edge node, to vertex 1,
        // because we need to have all vertexes generated first
        for ($i = 0; $i < $data['vertexesCount']; $i++) {
            $vertex = new Vertex();
            $vertex->setId($i);
            $this->addVertex($vertex);
        }

        foreach ($data['edges'] as $key => $edge) {
            foreach ($edge as $node) {
                $edgeNode = new EdgeNode();
                $edgeNode->setVertex($this->getVertexById($node[0]));
                $edgeNode->setWeight($node[1]);
                $this->getVertexById($key)->addEdgeNode($edgeNode);
            }
        }

    }

    public function printGraph()
    {
        if (empty($this->getVertexes())) {
            echo "<br> Graph's empty, nothing to print";
            return;
        }

        $output = '';
        foreach ($this->getVertexes() as $vertex) {
            $output .= "Vertex - {$vertex->getId()} ( ";
            $neighbours = $vertex->getEdgeNodes();
            if (null !== $neighbours) {
                foreach ($neighbours as $neighbour) {
                    $output .= "{$neighbour->getVertex()->getId()}, ";
                }
            }

            // deleting last coma;
            $output = substr($output, 0, -2);

            $output .= " ); <br>";

        }
        echo $output;
    }

    /**
     * Exports graph's data to parsable json, which you can use to load the graph.
     *
     * @return string
     */
    public function exportToParsableJson()
    {
        $data = [
            'vertexesCount' => sizeof($this->vertexes),
            'edges' => []
        ];

        foreach ($this->vertexes as $vertex) {
            foreach ($vertex->getEdgeNodes() as $edge) {
                $data['edges'][$vertex->getId()][] = [
                    $edge->getVertex()->getId(),
                    $edge->getWeight()
                ];
            }
        }

        return json_encode($data);
    }

    /**
     * Exports graph's data to json format which is most often used by JS libraries.
     *
     * @return string
     */
    public function exportToJson()
    {
        $data = [
            'nodes' => [],
            'edges' => [],
        ];

        foreach ($this->vertexes as $vertex) {
            $data['nodes'][] = [
                'data' => [
                    'id' => "{$vertex->getId()}",
                ]
            ];

            if (null !== $edgeNodes = $vertex->getEdgeNodes()) {
                foreach ($vertex->getEdgeNodes() as $edge) {
                    $edgeData = [
                        'data' => [
                            'source' => "{$vertex->getId()}",
                            'target' => "{$edge->getVertex()->getId()}",
                            'weight' => "{$edge->getWeight()}"
                        ]
                    ];

                    if (0 == $this->directed) {
                        $sortIDs = [$vertex->getId(),$edge->getVertex()->getId()];
                        sort($sortIDs);
                        $edgeData['data']['id'] = "{$sortIDs[0]}{$sortIDs[1]}";
                    }

                    $data['edges'][] = $edgeData;
                }
            }
        }

        return json_encode($data);
    }
}
