<?php

namespace Classes;

class EdgeNode
{
    /**
     * @var Vertex
     */
    protected $vertex;

    /**
     * @var int
     */
    protected $weight = 1;

    /**
     * @return Vertex
     */
    public function getVertex()
    {
        return $this->vertex;
    }

    /**
     * @param Vertex $vertex
     */
    public function setVertex(Vertex $vertex)
    {
        $this->vertex = $vertex;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
}
