<?php

namespace Classes;

class DepthFirstSearch
{
    /**
     * @var array
     */
    protected $visited = [];


    /**
     * @var Vertex[]
     */
    protected $sortedVertexes = [];

    /**
     * @var Graph
     */
    protected $graph;

    public function __construct(Graph $graph)
    {
        $this->graph = $graph;
    }

    /**
     * Visits all the nodes which are reachable from starting (provided $vertex) vertex.
     *
     * @param Vertex $vertex
     */
    public function visit(Vertex $vertex)
    {
        $this->addVisited($vertex);

        if (!empty($vertex->getEdgeNodes())) {
            foreach ($vertex->getEdgeNodes() as $node) {
                /** @var EdgeNode $node */
                if (!in_array($node->getVertex(), $this->visited)) {
                    $this->visit($node->getVertex());
                }
            }
        }

        // Adding vertexes for topological sort;
        array_unshift($this->sortedVertexes, $vertex);
    }

    /**
     * Visits all graph vertexes
     *
     * @param Graph $graph
     * @return $this
     */
    public function DFS()
    {
        if (empty($this->graph->getVertexes())) {
            echo "<br> Graph's empty, therefore can't do DFS!";
            return $this;
        }

        foreach ($this->graph->getVertexes() as $vertex) {
            if (!in_array($vertex, $this->visited)) {
                $this->visit($vertex);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getVisited()
    {
        return $this->visited;
    }

    /**
     * @param array $visited
     */
    public function setVisited($visited)
    {
        $this->visited = $visited;
    }

    /**
     * @param Vertex $vertex
     */
    public function addVisited(Vertex $vertex)
    {
        $this->visited[] = $vertex;
    }

    /**
     * @return Vertex[]
     */
    public function getSortedVertexes()
    {
        return $this->sortedVertexes;
    }

    /**
     * Prints DFS visited nodes
     */
    public function printDFSOutput()
    {
        $output = '';

        foreach ($this->getVisited() as $node) {
            $output .=  "{$node->getId()}, ";
        }

        $output = substr($output, 0, -2);

        echo $output;

        if (sizeof($this->getVisited()) < sizeof($this->graph->getVertexes())) {
            echo "<br> <u>Graph is not fully connected. this is the output of the graph's connected part</u>";
        }
    }
}
