<?php

namespace Classes;

class Kruskal
{
    /**
     * @var Graph
     */
    protected $graph;

    /**
     * @var array
     */
    protected $disjoints;

    /**
     * @var array
     */
    protected $edges;

    /**
     * @var array
     */
    protected $minimumSpanningTree;

    /**
     * @var int
     */
    protected $mspCost;

    public function __construct(Graph $graph)
    {
        $this->graph = $graph;
    }

    /**
     * Initiates the structure.
     */
    protected function init()
    {
        $saveEdges = [];

        foreach ($this->graph->getVertexes() as $vertex) {
            $this->disjoints[$vertex->getId()] = [$vertex->getId()];
            if (count($vertex->getEdgeNodes()) > 0) {
                foreach ($vertex->getEdgeNodes() as $edgeNode) {
                    // skipping duplicate edges in undirected graph.
                    if (!array_key_exists($vertex->getId().$edgeNode->getVertex()->getId(), $saveEdges)) {
                        $dataArray = [
                            'source' => $vertex->getId(),
                            'target' => $edgeNode->getVertex()->getId(),
                            'weight' => $edgeNode->getWeight()
                        ];
                        $this->edges[] = $dataArray;
                        $saveEdges[$dataArray['target'].$dataArray['source']] = null;
                    }
                }
            }
        }

        unset($saveEdges);

        usort($this->edges, function ($a, $b) {
            return $a['weight'] - $b['weight'];
        });
    }

    /**
     * Bootstrap method.
     *
     * @return $this
     */
    public function findMSP()
    {
        $this->init();

        $this->MSP();

        $this->calculateMSPCost();

        return $this;
    }

    public function MSP()
    {
        $counter = 0;

        $vertexCount = count($this->graph->getVertexes());
        $edgesCount = count($this->edges);

        for ($i = 0; $i < $edgesCount; $i++) {
            if ($counter == $vertexCount - 1) { // optimizing.
                // got the MSP ( edges = vertex - 1 )
                break;
            }

            $arrayA = null;
            $arrayB = null;

            foreach ($this->disjoints as $keyA => $valueA) {
                // if the two nodes ("trees") are in the same array ("forest") we do nothing.
                if (in_array($this->edges[$i]['source'], $valueA) &&
                    in_array($this->edges[$i]['target'], $valueA)
                ) {
                    break;
                }

                // if two nodes are in different arrays, we merge them.
                if (in_array($this->edges[$i]['source'], $valueA)) {
                    $arrayA = $valueA;
                    foreach ($this->disjoints as $keyB => $valueB) {
                        if (in_array($this->edges[$i]['target'], $valueB)) {
                            $arrayB = $valueB;
                            $this->disjoints[] = array_merge($arrayA, $arrayB);
                            unset($this->disjoints[$keyA]);
                            unset($this->disjoints[$keyB]);
                            $this->minimumSpanningTree[] = $this->edges[$i];
                            $counter++;
                            break;
                        }
                    }
                }
            }
        }

        return $this->minimumSpanningTree;
    }

    /**
     * Returns sum of MSP.
     *
     * @return int
     */
    public function calculateMSPCost()
    {
        $sum = 0;
        if (!empty($this->minimumSpanningTree)) {
            foreach ($this->minimumSpanningTree as $item) {
                $sum += $item['weight'];
            }
        }

        $this->mspCost = $sum;

        return $sum;
    }

    /**
     * @return Graph
     */
    public function getGraph()
    {
        return $this->graph;
    }

    /**
     * @param Graph $graph
     */
    public function setGraph($graph)
    {
        $this->graph = $graph;
    }

    /**
     * @return array
     */
    public function getDisjoints()
    {
        return $this->disjoints;
    }

    /**
     * @param array $disjoints
     */
    public function setDisjoints($disjoints)
    {
        $this->disjoints = $disjoints;
    }

    /**
     * @return array
     */
    public function getEdges()
    {
        return $this->edges;
    }

    /**
     * @param array $edges
     */
    public function setEdges($edges)
    {
        $this->edges = $edges;
    }

    /**
     * @return array
     */
    public function getMinimumSpanningTree()
    {
        return $this->minimumSpanningTree;
    }

    /**
     * @param array $minimumSpanningTree
     */
    public function setMinimumSpanningTree($minimumSpanningTree)
    {
        $this->minimumSpanningTree = $minimumSpanningTree;
    }

    /**
     * @return int
     */
    public function getMspCost()
    {
        return $this->mspCost;
    }

    /**
     * @param int $mspCost
     */
    public function setMspCost($mspCost)
    {
        $this->mspCost = $mspCost;
    }
}
