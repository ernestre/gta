<?php

require_once "bootstrap.php";

session_start();

$time  = time();
$fileName = "graph_{$time}.json";

header("Content-disposition: attachment; filename={$fileName}");
header('Content-type: application/json');

/**
 * @var \Classes\Graph $graph
 */

$graph = $_SESSION['graph'];

$graphData = $graph->exportToParsableJson();

file_put_contents("Data/json/{$fileName}", $graphData);

echo $graphData;
