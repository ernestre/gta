<?php

require_once "bootstrap.php";

session_start();

/**
 * @var Classes\Graph $graph
 */

$graph = $_SESSION['graph'];

/**
 * @var \Classes\Kruskal $kruskal
 */
$kruskal = $_SESSION['kruskal'];

$directed = true === $graph->isDirected() ? 1 : 0;

include_once "graphManagement.php";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="javascripts/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="javascripts/node_modules/cytoscape/dist/cytoscape.min.js"></script>
    <script src="javascripts/app.js"></script>

    <script>
        $(function(){
            var cy = cytoscape({
                container: $('#cy'),
                elements:
                <?=$graph->exportToJson();?>,
                style:cytoscape.stylesheet()
                    .selector('node')
                    .css({
                        'content': 'data(id)',
                        'text-valign': 'center',
                        'color': 'white',
                        'text-outline-width': 2,
                        'text-outline-color': '#888'
                    })
                    .selector('edge')
                    .css({
                        'content': 'data(weight)',
                        'font-size': 10,
                        'text-valign': 'center',
                        'color': 'white',
                        'text-outline-width': 2,
                        'text-outline-color': '#888',
                        'target-arrow-shape': 1==<?=$directed?> ? 'triangle' : 'none',
                    }),

                layout: {
                    name: 'circle',
                    padding: 10
                },

            });
        })

    </script>
    <style>
        #cy {
            width: 100%;
            height: 100vh;
            display: block;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <br>
            <button class="btn btn-primary" id="toggleGraphVertexes">Toggle Graph structure</button>
            <a href="download.php" class="btn btn-primary">Save graph (json)</a>
            <button class="btn btn-primary" id="toggleKruskalOutput">
                Toggle MSP (Kruskal)
            </button>

            <div id="graphVertexes" style="display: none;">
                <h3>
                    Graph structure :
                </h3>
                <code>
                    <?=$graph->printGraph()?>
                </code>
            </div>
            <div id="kruskalOutput" style="display: none">
                <h3>Kruskal MSP</h3>
                    <p>
                        MSP count : <strong><?=$kruskal->getMspCost() ?></strong>
                    </p>
                    <pre>
                        <?=print_r($kruskal->getMinimumSpanningTree())?>
                    </pre>
            </div>
            <div class="page-header">
                <h3>
                    DFS
                </h3>
            </div>
            <form action="" method="GET" class="form-horizontal" name="dfs">
                <div class="form-group">
                    <label for="dfsStartVertex" class="col-sm-3  control-label">Start DFS from vertex:</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" name="dfsStartVertex" id="dfsStartVertex" value="<?php !empty($_GET['dfsStartVertex']) ? print($_GET['dfsStartVertex']) : null ;?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-primary">Start DFS</button>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <a href="index.php" class="btn btn-danger"> Generate new graph</a>
                    </div>
                </div>
            </form>

            <code>
                DFS Result :
                <?php

                if (isset($_GET['dfsStartVertex']) && (int)$_GET['dfsStartVertex'] >= 0) {
                    $dfs = new \Classes\DepthFirstSearch($graph);
                    try {
                        $dfs->visit($graph->getVertexById((int)$_GET['dfsStartVertex']));
                        $dfs->printDFSOutput();
                    } catch (\OutOfBoundsException $e) {
                        echo "Can't start dfs from this vertex";
                    }
                }

                ?>
            </code>

            <!--     ADD NODE       -->
            <div class="page-header">
                <h3>Add nodes</h3>
            </div>

            <form action="" method="POST" class="form-horizontal">
                <div class="form-group">
                    <label for="newVertexCount" class="col-sm-3">Generate new vertexes</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="newVertexCount" name="newVertexCount">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
            <!--     ADD NODE       -->

            <!--     Delete NODE       -->
            <div class="page-header">
                <h3>Delete nodes</h3>
            </div>

            <form action="" method="POST" class="form-horizontal">
                <div class="form-group">
                    <label for="deleteVertexById" class="col-sm-3">Delete Node By id</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="deleteVertexById" name="deleteVertexById">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-danger">Delete</button>
                    </div>
                </div>
            </form>
            <!--     Delete NODE       -->

            <!--     ADD EDGE NODE       -->
            <div class="page-header">
                <h3>Add Edge nodes</h3>
            </div>

            <form action="" method="POST" class="form-horizontal">
                <div class="form-group">
                    <label for="edgeFrom" class="col-sm-3">Edge from vertex:</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="edgeFrom" name="edgeFrom">
                    </div>
                </div>

                <div class="form-group">
                    <label for="edgeTo" class="col-sm-3">Edge to vertex:</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="edgeTo" name="edgeTo">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
            <!--     //ADD EDGE NODE       -->

            <!--     DELETE EDGE NODE       -->
            <div class="page-header">
                <h3>Delete Edge nodes</h3>
            </div>

            <form action="" method="POST" class="form-horizontal">
                <div class="form-group">
                    <label for="delEdgeFrom" class="col-sm-3">Edge from vertex:</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="delEdgeFrom" name="delEdgeFrom">
                    </div>
                </div>

                <div class="form-group">
                    <label for="delEdgeTo" class="col-sm-3">Edge to vertex:</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control" id="delEdgeTo" name="delEdgeTo">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-danger">Delete</button>
                    </div>
                </div>
            </form>
            <!--     //DELETE EDGE NODE       -->

            <!--     graphStyle       -->
            <div class="page-header">
                <h3>Graph style</h3>
            </div>

            <form action="" method="POST" class="form-horizontal">

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="directed" <?php $graph->isDirected() ? print("checked") : null ;?>>
                                Graph is directed
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="undirected" <?php !$graph->isDirected() ? print("checked") : null ;?>>
                                Graph is undirected
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button class="btn btn-info">Change</button>
                    </div>
                </div>

            </form>
            <!--     //graphStyle       -->

        </div>
        <div class="col-sm-6">
            <div id="cy">
            </div>
        </div>
    </div>
</div>
</body>
</html>
