<?php

require_once "bootstrap.php";

session_start();

$graph = new \Classes\Graph();

if (isset($_POST['fileName'])) {
    // load from file
    $json = file_get_contents($_POST['fileName']);
    $graph->loadGraph($json);
} else {
    // generating random based on input
    $directed = false;

    if ($_POST['optionsRadios'] === 'directed') {
        $directed = true;
    }

    $graph->generateRandomGraph($_POST['vertexCount'], $_POST['minEdges'], $_POST['maxEdges'], $directed);
}

$_SESSION['graph'] = $graph;

// Kruskal
$kruskal = new \Classes\Kruskal($graph);
$_SESSION['kruskal'] = $kruskal->findMSP();

header("Location: display.php");
